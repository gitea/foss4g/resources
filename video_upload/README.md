This repository contain script useful to upload video on YouTube.

It is able to read a JSON from Pretalx search for the video in a folder, create the metadata for YouTube and for TIB (creating the associated XML file) from the Pretalx json item.

How to install
==============

Create a Python virtual environment and later install the required libraries using `pip`

    pip install -r requirements.txt

Create configuration file
=========================

Copy the `client.conf.example` and set the right parameters

Set Google API
==============

Go to https://console.cloud.google.com/apis/dashboard and later to `Credentials` set up a new OAuth 2.0 clicking on the button in the top of the page. At the end of the procedure you will get a client ID and secret.

To get the authorization code you need to use the browser asking for the following page (replacing CLIENT_ID with your ID)

    https://accounts.google.com/o/oauth2/v2/auth?client_id=YOUR_CLIENT_ID&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code&scope=https://www.googleapis.com/auth/youtube

You need to select FOSS4G (to test you can also yous yours) as account, at this point you get a code that you need to save into the configuration file under `youtube` section in the `code` parameter.

The first time you are going to run the script you need to save the Access Token and Refresh Token, printed from the script, to the configuration file under `youtube` section respectively into `access` and `refresh` parameters.

How to use
==========

First of all download the Pretalx JSON, set up the configuration file and enable the Google API.
When you have configured everything, you can test if the YouTube codes are correct launching the script

    python upload_video.py --tests

In this case it will look to `client.conf` file, if your configuration file name is different use the `--config_file` option

To upload the files you need just to launch the script, using `--config_file` option if needed, it is useful to use the `--debug` option to get more info about what is happening.

You can also run the script to obtain only the matadata (`--write_metadata`) or to create the thumbnail file (`--create_thumb`)