#!/usr/bin/python3
#    Copyright (C) 2017  andi, derpeter; 2022 lucadelu
#    lucadeluge gmail com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import datetime
import logging
import xml.etree.ElementTree as et
import json
import argparse
import glob
import mimetypes
import configparser
import tempfile
import requests
import slugify

try:
    from iso639 import Lang
except ImportError as exc:
    raise Exception("iso639 library is required") from exc

try:
    import moviepy.editor as mp
except ImportError:
    logging.warning("It is not possible to add conference logo to the video")
    mp = None

try:
    from wand.image import Image
    from wand.font import Font

    WAND = True
except ImportError:
    logging.warning("It is not possible edit thumbnail")
    WAND = False


class YouTubeException(Exception):
    """General YouTube extensions"""

    pass


def check_config_param(param, config, required=False):
    """Check if a parameter is null or is required

    Args:
        param (str): the value of the configuration parameter to check
        config (str): the section and parameter of the value
        required (bool, optional): Set true if that parameter is required. Defaults to False.

    Raises:
        ValueError: the value is required but it is not set

    Returns:
        The value
    """
    if param != "":
        return param
    if required:
        raise ValueError(f"Value for {config} is required")
    return None


class YoutubeAPI:
    """
    This class implements the YouTube API v3
    https://developers.google.com/youtube/v3/docs

    idea and part of the code taken from
    https://github.com/voc/voctopublish/blob/48b4b5bf25d6465838175d1412b25e16b29d359a/voctopublish/api_client/youtube_client.py
    """

    def __init__(self, config):
        # youtube configuration info
        self.client_id = config["youtube"]["client"]
        self.secret = config["youtube"]["secret"]
        self.playlist_id = config["youtube"]["playlist"]
        self.access_code = config["youtube"]["code"]
        self.youtube_category = config["youtube"]["category"]
        self.privacy = config["youtube"]["privacy"]
        self.channel_id = None
        if config["youtube"]["access"] != "":
            self.access_token = config["youtube"]["access"]
        else:
            self.access_token = None
            #self.access_token = "ya29.a0AX9GBdXZNI9laJ3m2t-a3kB1pSrsvzUg3gR7K5krkXtUbxBK464dVLFiiafRqUuhHuXh7_yJnlsYcGRrODdrBaA1F2gWydgpkxb2OYdtuQAz1B01Jk7y4DXgmPH8vmH1z4gjNHANxj1eysEiBt4l26UH6ks9aCgYKAVYSARESFQHUCsbCUyx8CNcgj5ZYMY9YLP3wqw0163"
            #self.refresh_token = "1//09ynhnWgQQBg_CgYIARAAGAkSNwF-L9Ir51L5oXgcDbGHTRi7pBRkVfIjSke1tD6dZt-YwQahBoaUStZ_RrpXVDvG6CqebtNeKAI"
        if config["youtube"]["refresh"] != "":
            self.refresh_token = config["youtube"]["refresh"]
        else:
            self.refresh_token = None
        # general configuration info
        self.conf_name = config["general"]["conf_name"]
        self.location = config["general"]["location"]
        self.logo = config["general"]["logo_path"]
        self.language = config["general"]["language"]
        self.short_lang = Lang(self.language).pt3
        self.license = config["general"]["cc"]
        self.license_url = config["general"]["cc_url"]
        self.pretalx = config["general"]["pretalx"]
        self.metadata = {}
        # thumbnail configuration info
        self.font = config["thumbnail"]["font_path"]
        self.font_size = float(config["thumbnail"]["font_size"])
        self.font_color = config["thumbnail"]["font_color"]
        if config["thumbnail"]["stroke_color"] != '':
            self.stroke_color = config["thumbnail"]["stroke_color"]
        else:
            self.stroke_color = None
        if config["thumbnail"]["stroke_size"] != '':
            self.stroke_size = float(config["thumbnail"]["stroke_size"])
        else:
            self.stroke_size = None
        if config["thumbnail"]["text_width"] != '':
            self.text_width = int(config["thumbnail"]["text_width"])
        else:
            self.text_width = None
        self.thumb_format = config["thumbnail"]["format"]
        self.thumb_template = config["thumbnail"]["template"]
        self.thumb_position = config["thumbnail"]["position"]

    def setup(self):
        """Fetch access token and channel if form youtube

        Args
            token (str): youtube token to be used
        """
        if not self.access_token:
            self.access_token, self.refresh_token = self.get_new_token(self.client_id, self.secret, self.access_code)
            access = self.access_token
            refresh = self.refresh_token
        else:
            access, refresh = self.get_fresh_token(self.refresh_token, self.client_id, self.secret)
        if access != self.access_token:
            print(f"New access token {access}")
            self.access_token = access
        if refresh != self.refresh_token:
            print(f"New refresh token {refresh}")
            self.refresh_token = refresh
        self.channel_id = self.get_channel_id(self.access_token)

    def add_logo(self, file):
        """Add logo to the file

        Args:
            file (str): full path to input video file
            logo (str): full path to input logo to add
        """
        if mp is None:
            logging.warning("moviepy not installed")
            return False
        video = mp.VideoFileClip(file)

        logo = (
            mp.ImageClip(self.logo)
            .set_duration(video.duration)
            .resize(height=100)  # if you need to resize...
            .margin(right=8, top=8, opacity=0)  # (optional) logo-border padding
            .set_pos(("right", "top"))
        )
        outfile = os.path.join(tempfile.gettempdir(), f"newlogo_{file}")
        final = mp.CompositeVideoClip([video, logo])
        final.write_videofile(outfile)
        return outfile

    def get_thumbnail_text(self, data):
        """Create the text to insert into thumbnail

        Args:
            data (dict): dictionary of a record from pretalx json output

        Returns:
            text: the the for the thumbnail
        """
        mytext = None
        if self.thumb_format == "title":
            mytext = data["Title"]
        elif self.thumb_format == "author":
            mytext = ",".join(data["Speaker names"])
        elif self.thumb_format == "title_author":
            mytext = "{tit}\n\n{aut}".format(
                tit=data["Title"], aut=",".join(data["Speaker names"])
            )
        return mytext

    def create_thumbnail(self, output, intext):
        """Fill a thumbnail template with text; optimized thumbnail input is 1280x720

        Args:
            output (str): the path output image file
            intext (str): the text to add to the thumbnail
        """
        myfont = Font(
            path=self.font,
            color=self.font_color,
            stroke_color=self.stroke_color,
            stroke_width=self.stroke_size,
            size=self.font_size,
        )
        with Image(filename=self.thumb_template) as img:
            with img.clone() as new:
                new.caption(text=intext, font=myfont, gravity=self.thumb_position, width=self.text_width)
                new.save(filename=output)
        logging.info(f"Thumbnail {output} created")

    def get_youtube_metadata(self, data):
        """Create metadata for Youtube from Pretalx JSON

        Args:
            data (dict): dictionary of a record from pretalx json output
        """
        title = data["Title"]
        try:
            subtitle = data["Subtitle"]
        except KeyError:
            subtitle = ""
        try:
            abstract = data["Abstract"]
        except KeyError:
            abstract = ""
        try:
            datat = datetime.datetime.strptime(data["Start"], "%Y-%m-%dT%H:%M:%S+00:00").isoformat()
        except KeyError:
            datat = ""
        try:
            room = data["Room"]["en"]
        except KeyError:
            room = list(data["Room"].values())[0]
        finally:
            room = ""
        try:
            speakers = ",".join(data["Speaker names"])
        except KeyError:
            speakers = ""

        try:
            track = data["Track"]["en"]
        except KeyError:
            track = ""

        description = "\n\n".join(
            [subtitle, abstract, speakers, datat, track, room]
        ).strip()

        metadata = {
            "kind": "youtube#video",
            "snippet": {
                "title": title,
                # YouTube does not allow <> in description -> escape them
                "description": description.replace("<", "&lt").replace(">", "&gt").replace("\r","\n"),
                "channelId": self.channel_id,
                #"tags": [],
                "defaultLanguage": self.short_lang[:2],
                "categoryId":  int(self.youtube_category),
                #"defaultAudioLanguage": self.short_lang[:2],
            },
            # "status": {
            #     "privacyStatus": self.privacy,
            #     "embeddable": True,
            #     "publicStatsViewable": True,
            #     "license": "creativeCommon",
            # },
            # "recordingDetails": {
            #     "recordingDate": datat,
            # },
        }

        # limit title length to 100 (YouTube api conformity)
        metadata["snippet"]["title"] = metadata["snippet"]["title"][:100]
 #       metadata["snippet"]["name"] = metadata["snippet"]["name"][:100]
        # limit Description length to 5000 (YouTube api conformity)
        metadata["snippet"]["description"] = metadata["snippet"]["description"][:5000]
        logging.info(f"Youtube metadata created for {title}")
        return metadata

    def get_tib_metadata(
        self,
        data,
        publishers,
        cc_license,
        cc_url,
        output=None,
        pretalx_url=None,
        duration=None,
        size=None,
        fformat=None,
    ):
        """Create XML metadata for TIB

        Args:
            data (dict): dictionary of a record from pretalx json output
            publishers (list): a list with publishers name, it could be conference
                               and/or organizations
            cc_license (str): shortname of the choosen Creative Commons License
            cc_url (str): URL of the choosen Creative Commons License
            output (str): path to the output file containing the XML
            pretalx_url (str): the prefix URL to the pretalx website for talks
                               like https://talks.osgeo.org/foss4g-2022/talk/
            duration (str): the duration of the video format HH:MM:SS
            size (str): the size of file
            fformat (str): the format of the file
        """
        inyear = data["Start"].split("T")[0].split("-")[0]
        root = et.Element("resource")
        root.set(
            "xsi:schemaLocation",
            "http://www.tib.eu/fileadmin/extern/knm/NTM-Metadata-Schema_v_2.5.xsd NTM-Metadata-Schema_v_2.5.xsd",
        )
        creators = et.Element("creators")
        root.append(creators)
        for author in data["Speaker names"]:
            creator = et.SubElement(creators, "creator")
            creator.text = author
        titles = et.Element("titles")
        root.append(titles)
        title = et.SubElement(titles, "title")
        title.text = data["Title"]

        if len(publishers) > 0:
            pubs = et.Element("publishers")
            root.append(pubs)
            for publi in publishers:
                pub = et.SubElement(pubs, "publisher")
                pub.text = publi

        year = et.Element("publicationYear")
        year.text = str(inyear)
        root.append(year)

        lang = et.Element("language")
        lang.text = self.short_lang
        root.append(lang)

        proyear = et.Element("productionYear")
        proyear.text = str(inyear)
        root.append(proyear)

        if self.location:
            proplace = et.Element("productionPlace")
            proplace.text = self.location
            root.append(proplace)

        genre = et.Element("genre")
        genre.text = "Conference/Talk"
        root.append(genre)

        subject = et.Element("subjectAreas")
        root.append(subject)
        sub = et.SubElement(subject, "subjectArea")
        sub.text = "Computer Science"

        descrs = et.Element("descriptions")
        root.append(descrs)
        descr = et.SubElement(descrs, "description")
        descr.set("descriptionType", "Abstract")
        descr.set("language", self.short_lang)
        descr.text = data["Abstract"]

        series = et.Element("series")
        root.append(series)
        serie_title = et.SubElement(series, "seriesTitle")
        if self.location:
            serie_title.text = f"{self.conf_name} {self.location} {inyear}"
        else:
            serie_title.text = f"{self.conf_name} {inyear}"
        if len(publishers) > 0:
            seriepubs = et.SubElement(series, "seriesPublisher")
            seriepubs.text = ", ".join(publishers)
        rights = et.Element("rights")
        rights.set("rightsURI", cc_url)
        rights.text = cc_license

        if pretalx_url:
            materials = et.Element("additionalMaterials")
            root.append(materials)
            mate = et.SubElement(materials, "additionalMaterial")
            mate.set("additionalMaterialType", "URL")
            mate.set("relationType", "isSupplementedTo")
            mate.set("additionalMaterialType", "URL")
            mate.text = f"{pretalx_url.rstrip('/')}/{data['ID']}"

        if duration:
            dur = et.Element("duration")
            root.append(dur)
            dur.text = duration

        if size:
            siz = et.Element("size")
            root.append(siz)
            siz.text = str(size)

        if fformat:
            form = et.Element("format")
            root.append(form)
            form.text = fformat
        try:
            et.indent(root, space="\t", level=0)
        except:
            print("indent function was added in Python 3.9")
        if output:
            with open(output, "w") as outfile:
                outfile.write(et.tostring(root, encoding="unicode"))
            logging.info(f"TIB XML file for {data['Title']} saved to {output}")
        else:
            print(str(et.tostring(root, encoding="unicode")))
        return True

    def upload(self, file, data):
        """
        Call the youtube API and push the file to youtube

        Args:
            file (obj): the video file to upload
            data (dict): JSON object from pretalx JSON output
        """

        metadata = self.get_youtube_metadata(data)
        tibfile = "{}_tib.xml".format(os.path.splitext(file)[0])
        self.get_tib_metadata(data=data, publishers=[
                    "OSGeo",
                ],
                cc_license=self.license,
                cc_url=self.license_url,
                output=tibfile,
                pretalx_url=self.pretalx,)
        (mimetype, encoding) = mimetypes.guess_type(file)
        size = os.stat(file).st_size

        logging.debug(
            f"guessed mime type for file {file} as {mimetype} and its size as {size} bytes"
        )

        # https://developers.google.com/youtube/v3/docs/videos#resource
        req = requests.post(
            "https://www.googleapis.com/youtube/v3/videos",
            params={
                "uploadType": "resumable",
                "part": "snippet"#,status,recordingDetails",
            },
            headers={
                "Authorization": "Bearer " + self.access_token,
                "Content-Type": "application/json; charset=UTF-8",
                "X-Upload-Content-Type": mimetype,
                "X-Upload-Content-Length": str(size),
            },
            data=json.dumps(metadata),
            timeout=1200,
        )

        if 200 != req.status_code:
            if 400 == req.status_code:
                raise YouTubeException(
                    req.json()["error"]["message"]
                    + "\n"
                    + req.text
                    + "\n\n"
                    + json.dumps(metadata, indent=2)
                )
            else:
                raise YouTubeException(
                    f"Video creation failed with error-code {req.status_code}: {req.text}"
                )

        if "location" not in req.headers:
            raise YouTubeException(
                f"Video creation did not return a location-header to upload to: {req.headers}"
            )

        logging.info(
            "successfully created video and received upload-url from %s"
            % (req.headers["server"] if "server" in req.headers else "-")
        )
        logging.debug("uploading video-data to %s" % req.headers["location"])

        with open(file, "rb") as fiopen:
            upload = requests.put(
                req.headers["location"],
                headers={
                    "Authorization": "Bearer " + self.access_token,
                    "Content-Type": mimetype,
                },
                data=fiopen,
                timeout=600,
            )

            if 200 != upload.status_code and 201 != upload.status_code:
                raise YouTubeException(
                    f"uploading video failed with error-code {req.status_code}: {req.text}"
                )

        video = upload.json()

        outjpg = os.path.join(
            tempfile.gettempdir(), "{}_youtube.jpg".format(file.split(".")[0])
        )

        if WAND:
            logging.debug(f"starting to create thumbnail for video {video['id']}")
            mytext = self.get_thumbnail_text(data)
            self.create_thumbnail(outjpg, mytext)
            YoutubeAPI.update_thumbnail(self.access_token, video["id"], outjpg)


        youtube_url = "https://www.youtube.com/watch?v=" + video["id"]
        logging.info(f"successfully uploaded video as {youtube_url}")

        self.add_to_playlist(video[id])
        return True

    def add_to_playlist(self, video_id: str):
        """Add the uploaded video to the playlist
        documentation: https://developers.google.com/youtube/v3/docs/playlistItems/insert

        Args:
            video_id (str): the video ID
        """
        req = requests.post(
            "https://www.googleapis.com/youtube/v3/playlistItems",
            params={"part": "snippet"},
            headers={
                "Authorization": "Bearer " + self.access_token,
                "Content-Type": "application/json; charset=UTF-8",
            },
            data=json.dumps(
                {
                    "snippet": {
                        "playlistId": self.playlist_id,  # required
                        "resourceId": {
                            "kind": "youtube#video",
                            "videoId": video_id,
                        },  # required
                    },
                },
            ),
            timeout=60,
        )

        if 200 != req.status_code:
            raise YouTubeException(
                f"Adding video to playlist failed with error-code {req.status_code}: {req.text}"
            )

        logging.info(f"video added to playlist: {self.playlist_id}")
        return True

    def get_playlist(self):
        """Get the playlist from its id
        currently a method to help with debugging --Andi, August 2016
        """
        req = requests.get(
            "https://www.googleapis.com/youtube/v3/playlists",
            params={"part": "snippet", "id": self.playlist_id},
            headers={
                "Authorization": "Bearer " + self.access_token,
                "Content-Type": "application/json; charset=UTF-8",
            },
            timeout=30,
        )

        if 200 != req.status_code:
            raise YouTubeException(
                f"Video add to playlist failed with error-code {req.status_code}: {req.text}"
            )

        logging.debug(json.dumps(req.json(), indent=4))
        return True

    @staticmethod
    def get_new_token(client_id: str, client_secret: str, code: str):
        """Request a 'fresh' youtube token

        Args:
            client_id (str): the ID of the client
            client_secret (str): the password of the client
            code (str): the code given by https://accounts.google.com/o/oauth2/v2/auth?

        Returns:
            YouTube access token
            YouTube refresh token
        """
        logging.debug(
            f"fetching new Access-Token on behalf of the authorization code {code}"
        )
        req = requests.post(
            "https://accounts.google.com/o/oauth2/token",
            data={
                "client_id": client_id,
                "client_secret": client_secret,
                "code": code,
                "grant_type": "authorization_code",
                "redirect_uri": "urn:ietf:wg:oauth:2.0:oob"
            },
        )

        if 200 != req.status_code:
            raise YouTubeException(
                f"fetching a new authToken failed with error-code {req.status_code}: {req.text}"
            )

        data = req.json()
        if "access_token" not in data:
            raise YouTubeException(
                f"fetching a fresh authToken did not return a access_token: {req.text}"
            )

        logging.info(f"successfully fetched Access-Token {data['access_token']}")
        logging.info(f"successfully fetched Refresh-Token {data['refresh_token']}")
        print(f"Save the following tokens in the configuration file\nAccess-Token {data['access_token']}\nRefresh-Token {data['refresh_token']}")
        return data["access_token"], data["refresh_token"]


    @staticmethod
    def get_fresh_token(refresh_token: str, client_id: str, client_secret: str):
        """Request a 'fresh' youtube token

        Args:
            refresh_token (str): the token to refresh
            client_id (str): the ID of the client
            client_secret (str): the password of the client

        Returns:
            YouTube access token
        """
        logging.debug(
            f"fetching fresh Access-Token on behalf of the refreshToken {refresh_token}"
        )
        req = requests.post(
            "https://accounts.google.com/o/oauth2/token",
            data={
                "client_id": client_id,
                "client_secret": client_secret,
                "refresh_token": refresh_token,
                "grant_type": "refresh_token",
            },
        )

        if 200 != req.status_code:
            raise YouTubeException(
                f"fetching a fresh authToken failed with error-code {req.status_code}: {req.text}"
            )

        data = req.json()
        if "access_token" not in data:
            raise YouTubeException(
                f"fetching a fresh authToken did not return a access_token: {req.text}"
            )

        logging.info(f"successfully fetched Access-Token {data['access_token']}")
        return data["access_token"], refresh_token

    @staticmethod
    def get_channel_id(access_token: str):
        """Request the channel id associated with the access token

        Args:
            access_token (str): Youtube access token

        Returns:
            YouTube channel id
        """
        logging.debug(
            f"fetching Channel-Info on behalf of the accessToken {access_token}"
        )
        req = requests.get(
            "https://www.googleapis.com/youtube/v3/channels",
            headers={
                "Authorization": "Bearer " + access_token,
            },
            params={
                "part": "id,brandingSettings",
                "mine": "true",
            },
        )

        if 200 != req.status_code:
            raise YouTubeException(
                f"fetching channelID failed with error-code {req.status_code}: {req.text}"
            )

        data = req.json()
        channel = data["items"][0]

        logging.info(f"successfully fetched Channel-ID {channel['id']}")
        return channel["id"]

    @staticmethod
    def update_thumbnail(access_token: str, video_id: str, thumbnail: str):
        """This function update the video thumbnail
        https://developers.google.com/youtube/v3/docs/thumbnails/set

        Args:
            access_token (str): the Youtube access token
            video_id (str): the ID of the video to update
            thumbnail (str): the path to the thumbnail
        """
        fiopen = open(thumbnail, "rb")

        req = requests.post(
            "https://www.googleapis.com/upload/youtube/v3/thumbnails/set",
            params={"videoId": video_id},
            headers={
                "Authorization": "Bearer " + access_token,
                "Content-Type": "image/png",
            },
            data=fiopen.read(),
        )

        if 200 != req.status_code:
            raise YouTubeException(
                f"Video update failed with error-code {req.status_code}: {req.text}"
            )

        logging.info(f"Thumbnails for {video_id} updated")
        return True


def get_input_path(config, data):
    """Function to return the right path variable could be {date},

    Args:
        config (obj): The
        data (dict):
    """
    path = config["general"]["video_path"]
    datat = ""
    room = ""
    if "{date}" in path:
        try:
            datat = data["Start"].split("T")[0]
        except KeyError:
            datat = ""
    if "{room}" in path:
        try:
            room = data["Room"]["en"]
        except KeyError:
            try:
                room = list(data["Room"].values())[0]
            except:
                pass
    path = path.format(room=room, date=datat)
    return path

def get_video_path(inpath, looking):
    """_summary_

    Args:
        inpath (str): input directory containing videos
        looking (str): text matching the

    Returns:
        _type_: _description_
    """
    tmppath = glob.glob(inpath + '*{}*'.format(looking))
    if len(tmppath) == 0:
        filepath = None
    elif len(tmppath) == 1:
        filepath = tmppath[0]
        logging.info(f"video file founded at {filepath}")
    elif len(tmppath) > 1:
        filepath = None
        logging.warning(f"more video founded for {looking}")
    return filepath

def main(argus):
    """Main function"""
    if argus.debug or argus.tests:
        logging.basicConfig(level=logging.DEBUG)
    config = configparser.ConfigParser(allow_no_value=True)
    config.read_file(open(argus.config_file))
    jfile = open(config["general"]["json_path"])
    jsondata = json.load(jfile)
    yta = YoutubeAPI(config)
    if argus.tests:
        yta.setup()
        if yta.channel_id:
            logging.info(f"Channel ID: {yta.channel_id}")
        if yta.get_playlist():
            logging.info(f"Got the right playlist ID: {yta.playlist_id}")
    if argus.create_thumb:
        if not WAND:
            raise ImportError("Please install wand library")
    for data in jsondata:
        mypath = get_input_path(config, data)
        if not os.path.exists(mypath):
            raise OSError(f"{mypath} does not exist")
        if argus.write_metadata:
            logging.info("Creating metadata")
            mytext = yta.get_thumbnail_text(data)
            with open(
                os.path.join(
                    mypath,
                    "{}_youtube_metadata.json".format(
                        slugify.slugify(mytext)#, separator="_")
                    ),
                ),
                "w",
            ) as ifile:
                ifile.write(json.dumps(yta.get_youtube_metadata(data), indent=4))
            tibfile = os.path.join(
                mypath,
                "{}_tib_metadata.xml".format(slugify.slugify(mytext))#, separator="_")),
            )
            yta.get_tib_metadata(
                data=data,
                publishers=[
                    "OSGeo",
                ],
                cc_license=config["tib"]["cc"],
                cc_url=config["tib"]["cc_url"],
                output=tibfile,
                pretalx_url=config["tib"]["pretalx"],
            )
        if argus.create_thumb:
            logging.info("Creating thumbnail")
            if not WAND:
                raise ImportError("Please install wand library")
            mytext = yta.get_thumbnail_text(data)
            outjpg = os.path.join(
                mypath, "{}_youtube.jpg".format(slugify.slugify(mytext))#, separator="_"))
            )
            yta.create_thumbnail(outjpg, mytext)

        if argus.create_thumb or argus.write_metadata:
            return True
        yta.setup()
        filepath = get_video_path(mypath, data["Title"])
        if not filepath:
            speakers = data['Speaker names']
            for speak in speakers:
                filepath = get_video_path(mypath, speak)
                if filepath:
                    break
        if not filepath:
            logging.warning(f"No video founded for {data['Title']}")
            continue
        if argus.add_logo:
            filepath = yta.add_logo(filepath)
        yta.upload(file=filepath, data=data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--config_file",
        "-c",
        default="client.conf",
        help="The configuration file; default ",
    )
    parser.add_argument(
        "--create_thumb", "-t", action="store_true", help="Create only the thumbnails"
    )
    parser.add_argument(
        "--write_metadata",
        "-w",
        action="store_true",
        help="Create metada for youtube and TiB",
    )
    parser.add_argument(
        "--add_logo",
        "-a",
        action="store_true",
        help="Add the conference logo to the video",
    )
    parser.add_argument(
        "--tests",
        "-s",
        action="store_true",
        help="Make test before upload",
    )
    parser.add_argument(
        "--debug",
        "-d",
        action="store_true",
        help="Set logging level to DEBUG, otherwise WARNING",
    )
    args = parser.parse_args()
    main(args)
